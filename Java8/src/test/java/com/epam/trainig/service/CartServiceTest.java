package com.epam.trainig.service;

import com.epam.trainig.CartService;
import com.epam.trainig.Entry;
import com.epam.trainig.Product;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CartServiceTest extends CartService {

    CartService cartService = new CartService();
    List<Entry> testEntries = new ArrayList<>();
    List<Entry> test = new ArrayList<>();

    @Test
    public void getCartEntries1() {
        assertNotNull(cartService.getCartEntries());
    }

    @Test
    public void cartSum1() {
        testEntries.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        testEntries.add(new Entry(6, new Product("code6", "apple", 30, Collections.singletonList("fruit")), 1001));
        double p = cartService.cartSum(testEntries);
        assertEquals(48, (int)p);
    }

    @Test
    public void isCartSumNotMoreThanThousand1() {
        testEntries.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        testEntries.add(new Entry(6, new Product("code6", "apple", 30, Collections.singletonList("fruit")), 11));
        boolean p = cartService.isCartSumNotMoreThanThousand(testEntries);
        assertTrue(p);
    }

    @Test
    public void isAnyQuantityMoreThanTen1() {
        testEntries.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        testEntries.add(new Entry(6, new Product("code6", "apple", 30, Collections.singletonList("fruit")), 11));
        boolean p = cartService.isAnyQuantityMoreThanTen(testEntries);
        assertTrue(p);
    }

    @Test
    public void getProductQuantityMoreThanThree1() {
        testEntries.add(new Entry(6, new Product("code6", "apple", 30, Collections.singletonList("fruit")), 11));
        test = cartService.getProductQuantityMoreThanThree(testEntries);
        assertEquals(testEntries, test);
    }

    @Test
    public void maxProductPrice1() {
        testEntries.add(new Entry(7, new Product("code7", "banana", 60, Collections.singletonList("shoes")), 7));
        testEntries.add(new Entry(8, new Product("code8", "orange", 30, Collections.singletonList("fruit")), 15));
        OptionalDouble od = cartService.maxProductPrice(testEntries);
        double p = od.getAsDouble();
        assertEquals(60, (int) p);
    }

    @Test
    public void minProductPrice1() {
        testEntries.add(new Entry(7, new Product("code7", "banana", 60, Collections.singletonList("shoes")), 7));
        testEntries.add(new Entry(8, new Product("code8", "orange", 30, Collections.singletonList("fruit")), 15));
        OptionalDouble od = cartService.minProductPrice(testEntries);
        double p = od.getAsDouble();
        assertEquals(30, (int) p);
    }

    @Test
    public void sortByPrice1() {
        testEntries.add(new Entry(1, new Product("code1", "milk", 20, Collections.singletonList("dairy")), 5));
        testEntries.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        testEntries.add(new Entry(3, new Product("code3", "bread", 22, Collections.singletonList("bakery")), 2));
        test.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        test.add(new Entry(1, new Product("code1", "milk", 20, Collections.singletonList("dairy")), 5));
        test.add(new Entry(3, new Product("code3", "bread", 22, Collections.singletonList("bakery")), 2));
        assertEquals(test, cartService.sortByPrice(testEntries));
    }

    @Test
    public void sortByProductNameReverse1() {
        testEntries.add(new Entry(10, new Product("code10", "pork", 107, Collections.singletonList("meet")), 3));
        testEntries.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        testEntries.add(new Entry(4, new Product("code4", "sour_cream", 25, Collections.singletonList("dairy")), 5));
        test.add(new Entry(4, new Product("code4", "sour_cream", 25, Collections.singletonList("dairy")), 5));
        test.add(new Entry(5, new Product("code5", "roll", 18, Collections.singletonList("bakery")), 5));
        test.add(new Entry(10, new Product("code10", "pork", 107, Collections.singletonList("meet")), 3));
        assertEquals(test, cartService.sortByProductNameReverse(testEntries));
    }

    @Test
    public void allProductsInCart1() {
        testEntries.add(new Entry(7, new Product("code7", "banana", 60, Collections.singletonList("shoes")), 7));
        testEntries.add(new Entry(8, new Product("code8", "orange", 30, Collections.singletonList("fruit")), 15));
        List<String> p = cartService.allProductsInCart(testEntries);
        List<String> expect = new ArrayList<String>();
        expect.add("banana");
        expect.add("orange");
        assertEquals(expect, p);
    }

    @Test
    public void getCategoryShoes1() {
        testEntries.add(new Entry(7, new Product("code7", "banana", 60, Collections.singletonList("shoes")), 7));
        testEntries.add(new Entry(8, new Product("code8", "orange", 30, Collections.singletonList("fruit")), 15));
        test = cartService.getCategoryShoes(testEntries);
        List<Entry> expect = new ArrayList<>();
        expect.add(testEntries.get(0));
        assertEquals(expect, test);
    }

    @Test
    public void getAllCategoriesInCart1() {
        List<String> expect = Arrays.asList("dairy", "bakery", "fruit", "shoes", "meet");
        assertEquals(expect, cartService.getAllCategoriesInCart(cartService.getCartEntries()));
    }
}