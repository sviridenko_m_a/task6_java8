package com.epam.trainig;

import org.apache.log4j.Logger;

public class Runner {
    public static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {

        CartService cartService = new CartService();

        LOGGER.info("\n"+ Constants.GET_ALL_CATEGORIES +"\n"+
                cartService.getCartEntries() +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.CART_SUM +"\n"+
                cartService.cartSum(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.IS_CART_SUM_NOT_MORE_THAN_THOUSAND +"\n"+
                cartService.isCartSumNotMoreThanThousand(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.IS_ANY_QUANTITY_MORE_THAN_TEN +"\n"+
                cartService.isAnyQuantityMoreThanTen(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.GET_PRODUCT_QUANTITY_MORE_THAN_THREE +"\n"+
                cartService.getProductQuantityMoreThanThree(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.MAX_PRODUCT_PRICE +"\n"+
                cartService.maxProductPrice(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.MIN_PRODUCT_PRICE +"\n"+
                cartService.minProductPrice(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.SORT_BY_PRICE +"\n"+
                cartService.sortByPrice(CartService.entries) + "\n"+ Constants.DELIMETER +"\n");

        LOGGER.info("\n"+ Constants.SORT_BY_PRODUCT_NAME_REVERCE +"\n"+
                cartService.sortByProductNameReverse(CartService.entries) + "\n"+ Constants.DELIMETER +"\n");

        LOGGER.info("\n"+ Constants.ALL_PRODUCTS_IN_CART +"\n"+
                cartService.allProductsInCart(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.GET_CATEGORY_SHOES +"\n"+
                cartService.getCategoryShoes(CartService.entries) +"\n"+ Constants.DELIMETER +"\n\n");

        LOGGER.info("\n"+ Constants.GET_ALL_CATEGORIES_IN_CART +"\n"+
                cartService.getAllCategoriesInCart(CartService.entries) +"\n"+ Constants.DELIMETER);

    }
}
