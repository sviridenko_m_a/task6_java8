package com.epam.trainig;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

public class CartService {
    private static final Logger LOGGER = Logger.getLogger(CartService.class.getName());
    static final List<Entry> entries = new ArrayList<>();

    public List<Entry> getCartEntries() {
        String[] str;
        try (Scanner sc = new Scanner(new FileReader("src/in.txt"))) {
            for (int i = 0; i < 10; i++) {
                if (sc.hasNextLine()) {
                    str = sc.nextLine().trim().split(" ");

                    int id = parseInt(str[0]);
                    String code = str[1];
                    String name = str[2];
                    double price = Double.parseDouble(str[3]);
                    String categories = str[4];
                    int quantity = parseInt(str[5]);

                    Product product = new Product(code, name, price, Collections.singletonList(categories));
                    Entry entry = new Entry(id, product, quantity);
                    entries.add(entry);
                }
            }
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Input file is not found");
        }
        return entries;
    }

    public double cartSum(List<Entry> entries) {
        return entries.stream().mapToDouble((p) -> p.getProduct().getPrice()).sum();
    }

    public boolean isCartSumNotMoreThanThousand(List<Entry> entries) {
        Predicate<Double> pr = (s) -> s <= 1000;
        return pr.test(cartSum(entries));
    }

    public boolean isAnyQuantityMoreThanTen(List<Entry> entries) {
        return entries.stream().anyMatch((p) -> p.getQuantity() > 10);
    }

    public List<Entry> getProductQuantityMoreThanThree(List<Entry> entries) {
        return entries.stream().filter((p) -> p.getQuantity() > 3).collect(Collectors.toList());
    }

    public OptionalDouble maxProductPrice(List<Entry> entries) {
        return entries.stream().mapToDouble((p) -> p.getProduct().getPrice()).max();
    }

    public OptionalDouble minProductPrice(List<Entry> entries) {
        return entries.stream().mapToDouble((p) -> p.getProduct().getPrice()).min();
    }

    public List<Entry> sortByPrice(List<Entry> entries) {
        return entries.stream().sorted(new Entry()).collect(Collectors.toList());
    }

    public List<Entry> sortByProductNameReverse(List<Entry> entries) {
        return entries.stream().sorted(Comparator.comparing(entry -> ((Entry) entry).getProduct().getName()).reversed()).collect(Collectors.toList());
    }

    public List<String> allProductsInCart(List<Entry> entries) {
        return entries.stream().map((p) -> p.getProduct().getName()).collect(Collectors.toList());
    }

    public List<Entry> getCategoryShoes(List<Entry> entries) {
        List<String> equalCategory = new ArrayList<>();
        equalCategory.add("shoes");
        return entries.stream().filter((p) -> equalCategory.equals(p.getProduct().getCategories())).collect(Collectors.toList());
    }

    public List<String> getAllCategoriesInCart(List<Entry> entries) {
       return entries.stream().map(p -> p.getProduct()).flatMap(o -> o.getCategories().stream()).distinct().collect(Collectors.toList());
    }
}
