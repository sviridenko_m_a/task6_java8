package com.epam.trainig;

import java.util.Comparator;
import java.util.Objects;

public class Entry implements Comparator<Entry> {

    private int id;
    private Product product;
    private int quantity;

    public Entry() {
        this(0, null, 0);
    }

    public Entry(int id, Product product, int quantity) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public int compare(Entry o1, Entry o2) {
        return (int) (o1.getProduct().getPrice() - o2.getProduct().getPrice());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry entry = (Entry) o;
        return id == entry.id ||
                quantity == entry.quantity ||
                Objects.equals(product, entry.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product, quantity);
    }

    @Override
    public String toString() {
        return id + ", " + product + ", " + quantity + "\n";
    }
}
