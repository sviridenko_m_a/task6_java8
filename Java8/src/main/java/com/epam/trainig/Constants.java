package com.epam.trainig;

final class Constants {

    static final String DELIMETER = "-------------------------------------------------------";
    static final String GET_ALL_CATEGORIES = "1. Get a collection of products in cart.";
    static final String CART_SUM = "2. Get cart total price.";
    static final String IS_CART_SUM_NOT_MORE_THAN_THOUSAND = "3. Сheck that no entry total does not exceed 1000.";
    static final String IS_ANY_QUANTITY_MORE_THAN_TEN = "4. Сheck that at least 1 entry quantity > 10.";
    static final String GET_PRODUCT_QUANTITY_MORE_THAN_THREE = "5. Get entry with quantity > 3.";
    static final String MAX_PRODUCT_PRICE = "6. Get max product price in cart.";
    static final String MIN_PRODUCT_PRICE = "6. Get min product price in cart.";
    static final String SORT_BY_PRICE = "7. Sort entry by asc product price.";
    static final String SORT_BY_PRODUCT_NAME_REVERCE = "8. Sort entry by desc product name.";
    static final String ALL_PRODUCTS_IN_CART = "9. Get list all product names in cart.";
    static final String GET_CATEGORY_SHOES = "10. Get all entries with product category “Shoes”.";
    static final String GET_ALL_CATEGORIES_IN_CART = "11. Get unique collection of all categories in cart.";
}
