package com.epam.trainig;

import java.util.List;
import java.util.Objects;

public class Product {

    private String code;
    private String name;
    private double price;
    private List<String> categories;

    public Product() {
        this(null, null, 0.0, null);
    }

    public Product(String code, String name, double price, List<String> categories) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.categories = categories;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public List<String> getCategories() {

        return categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                Objects.equals(code, product.code) &&
                Objects.equals(name, product.name) &&
                Objects.equals(categories, product.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, price, categories);
    }

    @Override
    public String toString() {
        return code + ", " + name + ", " + price + ", " + categories;
    }
}
