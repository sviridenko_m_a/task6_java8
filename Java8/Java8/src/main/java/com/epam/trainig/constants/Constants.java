package com.epam.trainig.constants;

public final class Constants {

    public static final String DELIMETER = "\n-------------------------------------------------------\n";
    public static final String GET_ALL_CATEGORIES = "\n1. Get a collection of products in cart.\n";
    public  static final String CART_SUM = "\n2. Get cart total price.\n";
    public  static final String NO_ENTRY_TOTAL_MORE_THAN_THOUSAND = "\n3. Сheck that no entry total does not exceed 1000.\n";
    public  static final String IS_ANY_QUANTITY_MORE_THAN_TEN = "\n4. Сheck that at least 1 entry quantity > 10.\n";
    public  static final String GET_PRODUCT_QUANTITY_MORE_THAN_THREE = "\n5. Get entry with quantity > 3.\n";
    public  static final String MAX_PRODUCT_PRICE = "\n6. Get max product price in cart.\n";
    public static final String MIN_PRODUCT_PRICE = "\n6. Get min product price in cart.\n";
    public static final String SORT_BY_PRICE = "\n7. Sort entry by asc product price.\n";
    public static final String SORT_BY_PRODUCT_NAME_REVERCE = "\n8. Sort entry by desc product name.\n";
    public static final String ALL_PRODUCTS_IN_CART = "\n9. Get list all product names in cart.\n";
    public static final String GET_CATEGORY_SHOES = "\n10. Get all entries with product category “Shoes”.\n";
    public static final String GET_ALL_CATEGORIES_IN_CART = "\n11. Get unique collection of all categories in cart.\n";
}
