package com.epam.trainig.service;

import com.epam.trainig.beans.Entry;
import com.epam.trainig.beans.Product;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

public class CartService {
    private static final Logger LOGGER = Logger.getLogger(CartService.class.getName());
    public final List<Entry> entries = new ArrayList<>();

    public CartService(){
        String[] str;
        try (Scanner sc = new Scanner(new FileReader("src/in.txt"))) {
            for (int i = 0; i < 10; i++) {
                if (sc.hasNextLine()) {
                    str = sc.nextLine().trim().split(" ");

                    int id = parseInt(str[0]);
                    String code = str[1];
                    String name = str[2];
                    double price = Double.parseDouble(str[3]);
                    String categories = str[4];
                    int quantity = parseInt(str[5]);

                    Product product = new Product(code, name, price, Collections.singletonList(categories));
                    Entry entry = new Entry(id, product, quantity);
                    entries.add(entry);
                }
            }
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Input file is not found");
        }
    }

    public double cartSum(List<Entry> entries) {
        return entries.stream()
                .mapToDouble(entry -> entry.getProduct().getPrice()).sum();
    }

    public boolean noEntryTotalMoreThanThousand(List<Entry> entries) {
        return entries.stream()
                .noneMatch(entry -> entry.getProduct().getPrice() * entry.getQuantity() > 1000);
    }

    public boolean isAnyQuantityMoreThanTen(List<Entry> entries) {
        return entries.stream()
                .anyMatch(entry -> entry.getQuantity() > 10);
    }

    public List<Entry> getProductQuantityMoreThanThree(List<Entry> entries) {
        return entries.stream()
                .filter(entry -> entry.getQuantity() > 3)
                .collect(Collectors.toList());
    }

    public OptionalDouble maxProductPrice(List<Entry> entries) {
        return entries.stream()
                .mapToDouble(entry -> entry.getProduct().getPrice())
                .max();
    }

    public OptionalDouble minProductPrice(List<Entry> entries) {
        return entries.stream()
                .mapToDouble(entry -> entry.getProduct().getPrice())
                .min();
    }

    public List<Entry> sortByPrice(List<Entry> entries) {
        return entries.stream()
                .sorted(Comparator.comparingDouble(entry -> entry.getProduct().getPrice()))
                .collect(Collectors.toList());
    }

    public List<Entry> sortByProductNameReverse(List<Entry> entries) {
        return entries.stream()
                .sorted(Comparator.comparing(entry -> ((Entry) entry).getProduct().getName())
                .reversed())
                .collect(Collectors.toList());
    }

    public List<String> getProductNamesInCart(List<Entry> entries) {
        return entries.stream()
                .map(entry -> entry.getProduct().getName())
                .collect(Collectors.toList());
    }

    public List<Entry> getAllEntriesWithCategory(String category) {
        return entries.stream()
                .filter(entry -> entry.getProduct().getCategories().contains(category))
                .collect(Collectors.toList());
    }

    public List<String> getAllCategoriesInCart(List<Entry> entries) {
       return entries.stream()
               .map(Entry::getProduct)
               .flatMap(entry -> entry.getCategories().stream())
               .distinct()
               .collect(Collectors.toList());
    }
}
