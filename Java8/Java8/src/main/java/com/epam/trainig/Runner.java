package com.epam.trainig;

import com.epam.trainig.constants.Constants;
import com.epam.trainig.service.CartService;
import org.apache.log4j.Logger;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {

        CartService cartService = new CartService();

        LOGGER.info(Constants.GET_ALL_CATEGORIES +
                cartService.entries + Constants.DELIMETER);

        LOGGER.info(Constants.CART_SUM +
                cartService.cartSum(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.NO_ENTRY_TOTAL_MORE_THAN_THOUSAND +
                cartService.noEntryTotalMoreThanThousand(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.IS_ANY_QUANTITY_MORE_THAN_TEN +
                cartService.isAnyQuantityMoreThanTen(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.GET_PRODUCT_QUANTITY_MORE_THAN_THREE +
                cartService.getProductQuantityMoreThanThree(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.MAX_PRODUCT_PRICE +
                cartService.maxProductPrice(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.MIN_PRODUCT_PRICE +
                cartService.minProductPrice(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.SORT_BY_PRICE + cartService.sortByPrice(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.SORT_BY_PRODUCT_NAME_REVERCE +
                cartService.sortByProductNameReverse(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.ALL_PRODUCTS_IN_CART +
                cartService.getProductNamesInCart(cartService.entries) + Constants.DELIMETER);

        LOGGER.info(Constants.GET_CATEGORY_SHOES +
                cartService.getAllEntriesWithCategory("shoes") + Constants.DELIMETER);

        LOGGER.info(Constants.GET_ALL_CATEGORIES_IN_CART +
                cartService.getAllCategoriesInCart(cartService.entries) + Constants.DELIMETER);

    }
}
